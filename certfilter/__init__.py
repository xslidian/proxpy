#  This file is part of proxpy.
#
#  proxpy is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  proxpy is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with proxpy.  If not, see <https://www.gnu.org/licenses/>.

from fnmatch import fnmatchcase

def certfilter(cert):
    '''Certificate filter
    cert is a instance of http0._cert.Cert
    if this function return a True value, the certificate will be rejected.

    e.g.
    to reject any certificate issued by a common name start with WoTrus:
    return [cn for cn in cert.issuer.cn if fnmatchcase(cn,'WoTrus*')]
    '''
    return []

# Local Variables:
# coding: utf-8
# mode: python
# python-indent-offset: 4
# indent-tabs-mode: nil
# End:
