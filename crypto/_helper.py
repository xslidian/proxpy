#  This file is part of proxpy.
#
#  proxpy is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  proxpy is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with proxpy.  If not, see <https://www.gnu.org/licenses/>.

from nacl.signing import SigningKey,VerifyKey

def _str2bytes(s):
    if isinstance(s,str):
        return s.encode('utf8')
    return s

def _bytes2str(b):
    if isinstance(b,bytes):
        return b.decode('utf8')
    return b

def _hex2bytes(s):
    if isinstance(s,str):
        return bytes.fromhex(s)
    return s

def _bytes2hex(b):
    if isinstance(b,bytes):
        return b.hex()
    return b

def _output(key,as_hex=False):
    if as_hex:
        return bytes(key).hex()
    return bytes(key)

def _as_signing_key(key):
    return SigningKey(_hex2bytes(key))

def _as_verify_key(key):
    return VerifyKey(_hex2bytes(key))

def _as_private_key(key):
    return _as_signing_key(key).to_curve25519_private_key()

def _as_public_key(key):
    return _as_verify_key(key).to_curve25519_public_key()

# Local Variables:
# coding: utf-8
# mode: python
# python-indent-offset: 4
# indent-tabs-mode: nil
# End:
