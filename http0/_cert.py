#  This file is part of proxpy.
#
#  proxpy is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  proxpy is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with proxpy.  If not, see <https://www.gnu.org/licenses/>.

class CertRDN:
    '''Relative distinguished names (RDNs) of certificate.
    access every name by attribute using case-insensitive a abbr:
        C: countryName
        ST: stateOrProvinceName
        L: localityName
        O: organizationName
        OU: organizationalUnitName
        CN: commonName
    name is returned as iterable.
    '''

    _fieldmap=(
        ('C','countryName'),
        ('ST','stateOrProvinceName'),
        ('L','localityName'),
        ('O','organizationName'),
        ('OU','organizationalUnitName'),
        ('CN','commonName'),
    )

    def __init__(self,rdn):
        self._rdn=rdn

    def __getattr__(self,attr):
        if self._rdn is None:return
        name=dict(self._fieldmap).get(attr.upper(),None)
        if name is None:return
        yield from self._get_value(name)

    def _get_value(self,name):
        for line in self._rdn:
            yield from (v for k,v in line if k==name)

    def __iter__(self):
        for field,name in self._fieldmap:
            value='|'.join(self._get_value(name))
            if value:
                yield f'{field}={value}'

    def __len__(self):
        return len([s for s in self])

    def __bool__(self):
        return self._rdn is not None and len(self)>0

    def __repr__(self):
        return ', '.join(self)

class CertSubjectAltName:
    '''Subject Alternative Name extension of certificate
    access IP or DNS by case-insensitive attribute, returned as iterable.
    '''

    _fieldmap=(
        ('DNS','DNS'),
        ('IP','IP Address'),
    )

    def __init__(self,subjectAltName):
        self._alt=subjectAltName

    def __getattr__(self,attr):
        if self._alt is None:return
        name=dict(self._fieldmap).get(attr.upper(),None)
        if name is None:return
        yield from (v for k,v in self._alt if k==name)

    def __iter__(self):
        yield from (f'{k}:{v}' for k,v in self._alt)

    def __bool__(self):
        return self._alt is not None

    def __repr__(self):
        return ', '.join(self)

class Cert:
    '''Peer Certificate
    access information using attribute 'subject', 'issuer' and 'subjectAltName'.
    '''
    def __init__(self,cert):
        self._cert=cert

    @property
    def issuer(self):
        return CertRDN(self._cert.get('issuer',None))

    @property
    def subject(self):
        return CertRDN(self._cert.get('subject',None))

    @property
    def subjectAltName(self):
        return CertSubjectAltName(self._cert.get('subjectAltName',None))

    def __bool__(self):
        return self._cert is not None

    def __iter__(self):
        yield from (name for name in ('issuer','subject','subjectAltName')
                    if getattr(self,name))

    def __repr__(self):
        return '; '.join(f'{name}: {getattr(self,name)!r}' for name in self)

# Local Variables:
# coding: utf-8
# mode: python
# python-indent-offset: 4
# indent-tabs-mode: nil
# End:
