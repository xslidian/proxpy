#  This file is part of proxpy.
#
#  proxpy is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  proxpy is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with proxpy.  If not, see <https://www.gnu.org/licenses/>.

from ipaddress import ip_address
from ssl import SSLCertVerificationError

class SSLCertFiltered(SSLCertVerificationError):
    '''Exception raised when cert is filtered.'''
    def __init__(self,cert=None):
        self._cert=cert

    @property
    def verify_message(self):
        '''Filtered certificate infomation'''
        return f'certificate filtered: {self._cert!r}'

class SSLInvalidSubjectAlternativeName(SSLCertVerificationError):
    '''Exception raised when cert has a invalid subject alternative name.'''
    def __init__(self,name=''):
        self._name=name

    @property
    def verify_message(self):
        return f'Invalid subject alternative name: {self._name}.'

class SSLCertVerificationFailed(SSLCertVerificationError):
    '''Exception raised when cert verificate failed.'''
    def __init__(self,servername=''):
        self._servername=servername

    @property
    def verify_message(self):
        return f'Hostname mismatch, cert is not valid for {self._servername}.'

def isipaddr(host):
    '''Check whether host is a valid IPv4/IPv6 address.'''
    try:
        ipaddr=ip_address(host)
    except ValueError:
        return False
    else:
        if ipaddr.version in (4,6):
            return True
        return False

def _verify_hostname(hostname,pattern):
    head,dot,tail=pattern.lower().strip('.').partition('.')
    if '*' in head and head!='*':raise SSLInvalidSubjectAlternativeName(pattern)
    if '*' in tail:raise SSLInvalidSubjectAlternativeName(pattern)
    if head=='*' and not tail:raise SSLInvalidSubjectAlternativeName(pattern)
    servhead,dot,servtail=hostname.lower().strip('.').partition('.')
    if not tail and pattern.lower()!=hostname.lower():return
    if tail and tail!=servtail:return
    if head!='*' and head!=servhead:return
    return True

def verify_server_name(cert,server_name):
    '''Check whether the server_name in cert, raise if none matched.'''
    is_ip=isipaddr(server_name)
    if cert.subjectAltName:
        if is_ip:
            if server_name.lower() in map(str.lower,cert.subjectAltName.ip):
                return
        else:
            for pattern in cert.subjectAltName.dns:
                if _verify_hostname(server_name,pattern):
                    return
    for cn in cert.subject.cn:
        if _verify_hostname(server_name,cn):
            return
    raise SSLCertVerificationFailed(server_name)

# Local Variables:
# coding: utf-8
# mode: python
# python-indent-offset: 4
# indent-tabs-mode: nil
# End:
