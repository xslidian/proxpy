#  This file is part of proxpy.
#
#  proxpy is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  proxpy is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with proxpy.  If not, see <https://www.gnu.org/licenses/>.

from http.client import HTTP_PORT,HTTPS_PORT
from http.client import HTTPConnection as _HTTPConnection
from io import BytesIO
from ssl import SSLContext,SSLError,SSLCertVerificationError
from urllib.parse import SplitResult

from ._cert import Cert
from ._ssl_helper import SSLCertFiltered,verify_server_name,isipaddr

def _str2bytes(s):
    if isinstance(s,str):
        s=s.encode('utf8')
    return s

class NameResolveFailed(Exception):
    def __init__(self,hostname,reason=None):
        self.hostname=hostname
        self.reason=reason
    def __str__(self):
        return f'resolve failed: {self.hostname} ({self.reason})'

class NameNotFound(NameResolveFailed):
    def __str__(self):
        return f'name not found: {self.hostname}'

class HTTPConnection(_HTTPConnection):
    '''Connect to host using http
    If port is None, use default port of http(s).
    If hostname is not None, set as 'Host' header.
    If context is a SSLContext, connect using https.
    If tlsname is not None, check certificate with it as server_hostname
    Use proxy from proxy_host:proxy_port, send with proxy_headers.
    If dnsclient is a DNSClient and host is not ip address, resolve host with it.
    If certfilter, called with peer's Cert, return non-False, reject connection.
    If sni is False, Server Name Indication will be disabled.
    uid is the identify of conection.
    '''

    def __init__(self,host,/,port=None,*,
                 hostname=None,tlsname=None,
                 context=None,timeout=15,blocksize=8192,
                 proxy_host=None,proxy_port=None,proxy_headers={},
                 dnsclient=None,certfilter=None,sni=True,uid=None):

        self.https=isinstance(context,SSLContext)

        try:
            result=SplitResult(scheme=None,path=None,query=None,fragment=None,
                               netloc=host)
            if port is not None:
                port=int(port)
            if None not in (port,result.port) and port!=result.port:
                raise RuntimeError(f'invalid host and port: {host!r}, {port!r}')
            if port is None:
                port=result.port
            if port is None:
                port=HTTPS_PORT if self.https else HTTP_PORT
        except ValueError:
            raise RuntimeError(f'invalid host and port: {host!r}, {port!r}')

        if (proxy_host or proxy_port) and None in (proxy_host,proxy_port):
            raise RuntimeError('either host or port should not be None for proxy')

        self.sock=None
        self.default_headers={}
        # inited with 0, 5xx for connect failed
        self.status=0
        self.response_headers={}
        # BytesIO or HTTPResponse
        self.response_io=None

        self.uid=uid
        self.errmsg=None

        self._inited=False
        self._host=host
        self._port=port
        self._timeout=timeout
        self._blocksize=blocksize
        self._proxy_host=proxy_host
        self._proxy_port=proxy_port
        self._proxy_headers=proxy_headers
        self._dnsclient=dnsclient
        self._certfilter=certfilter
        self._sni=sni

        self._tlsctx=context if self.https else None
        self._tlsname=tlsname or host if self.https else None
        if hostname is not None:
            self.default_headers['host']=hostname
        elif tlsname is not None:
            self.default_headers['host']=tlsname
        self._remote_site=hostname or tlsname or host
        self._remote_port=port

    def __enter__(self):
        return self

    def __exit__(self,exc_type,exc_value,traceback):
        self.close()

    def _resolv(self):
        if self._dnsclient is None:
            return
        if isipaddr(self._host):
            return
        try:
            result=self._dnsclient.lookup(self._host,disable_ipv6=True)
        except Exception as e:
            raise NameResolveFailed(self._host,reason=e)
        try:
            ip=result[0][0]
        except IndexError:
            raise NameResolveFailed(self._host)
        if 'host' not in self.default_headers:
            self.default_headers['host']=self._host
        self._host=ip

    @property
    def headers(self):
        '''Short cut of response_headers.'''
        return dict((k,v) for k,v in self.response_headers.items())

    @property
    def remote_site(self):
        '''Hostname (or ip if resolved by DNSClient) of remote peer.'''
        return self._remote_site

    @property
    def remote_port(self):
        '''Port of remote peer.'''
        return self._remote_port

    @property
    def closed(self):
        return not self._inited

    def connect(self):
        '''Connect to remote peer.
        Will be called automatically before request if not connected.
        '''
        try:
            if not self._inited:
                self._resolv()
                super().__init__(self._host,self._port,
                                 timeout=self._timeout,
                                 blocksize=self._blocksize)
                self._inited=True
                if self._proxy_host and self._proxy_port:
                    super().set_tunnel(self._proxy_host,port=self._proxy_port,
                                       headers=self._proxy_headers)
            super().connect()
            if self.https:
                if not self._sni:
                    self._tlsctx.check_hostname=False
                self.sock=self._tlsctx.wrap_socket(
                    self.sock,
                    server_hostname=self._tlsname if self._sni else None)
                if callable(self._certfilter):
                    peercert=Cert(self.sock.getpeercert())
                    if self._certfilter(peercert):
                        raise SSLCertFiltered(peercert)
                if not self._sni:
                    verify_server_name(peercert,self._tlsname)
        except NameResolveFailed as e:
            self.errmsg=_str2bytes(f'{e}')
            self.close()
            self.status=600
        except SSLCertVerificationError as e:
            self.errmsg=_str2bytes(f'{e.verify_message}')
            self.close()
            self.status=611
        except SSLError as e:
            self.errmsg=_str2bytes(f'({e.library}) {e.reason}')
            self.close()
            self.status=612
        except Exception as e:
            # TODO: deal with common network error
            self.errmsg=_str2bytes(f'({type(e)}) {e}')
            self.close()
            self.status=601

    def clear(self):
        '''Clear received data and status.'''
        self.status=0
        self.response_headers.clear()
        respio=self.response_io
        if respio is not None:
            self.response_io=None
            respio.close()

    def close(self):
        '''Close connection and clear.'''
        if self._inited:
            super().close()
        self.clear()
        self._inited=False

    def response_iter(self,blocksize=4096):
        '''Iterable of received data'''
        if self.response_io is None:return
        while block:=self.response_io.read(blocksize):
            yield block

    def request(self,method,path,/,*,body=None,headers={},encode_chunked=False):
        '''Do a request.'''
        if self.sock is None:
            self.connect()
        if (status:=self.status)>599:
            # failed in connect
            self.close()
            self.status=status
            self.response_headers['content-length']=len(self.errmsg)
            self.response_io=BytesIO(self.errmsg)
            return
        self.clear()
        headers.update(self.default_headers)
        try:
            super().request(method,path,body=body,headers=headers,
                            encode_chunked=encode_chunked)
            # accept 3xx
            response=self.getresponse()
            self.status=int(response.status)
            self.response_headers.update(
                (k.lower(),v) for k,v in response.getheaders())
            self.response_io=response
        except Exception as e:
            # TODO: deal with common error
            errmsg=_str2bytes(f'({type(e)}) {e}')
            self.clear()
            self.status=602
            self.response_headers['content-length']=len(errmsg)
            self.response_io=BytesIO(errmsg)

# Local Variables:
# coding: utf-8
# mode: python
# python-indent-offset: 4
# indent-tabs-mode: nil
# End:
