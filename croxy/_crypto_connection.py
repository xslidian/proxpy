#  This file is part of proxpy.
#
#  proxpy is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  proxpy is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with proxpy.  If not, see <https://www.gnu.org/licenses/>.

from functools import partial
from json import dumps,loads
from pathlib import PurePosixPath
from random import randrange,sample
from ssl import SSLContext
from string import ascii_letters,digits
import struct
from urllib.parse import urlparse as _urlparse

import crypto
from http0 import HTTPConnection

from streampacker import stream_unpack

TOKEN_HEADER_FMT='<LL'

STRS=ascii_letters+digits+'_'

def _randstr(start=16,end=63):
    return ''.join(sample(STRS,randrange(start,end)))

def _park_token(key,sig):
    hdr=struct.pack(TOKEN_HEADER_FMT,len(key),len(sig))
    return hdr+key+sig

def urlparse(url):
    return _urlparse(url,allow_fragments=False)

def iterbytes(data,blocksize=4096):
    for n in range(0,len(data),blocksize):
        yield data[n:n+blocksize]

class Remote:
    def __init__(self):
        self.host=None
        self.port=None
        self.path=None
        self.hostname=None
        self.tlsname=None
        self.pubkey=None
        self.https=None
        self.context=None
        self.sni=None
        self.client_prikey=None
        self.signature=None

    def close(self):
        self.host=None
        self.client_prikey=None
        self.signature=None

class CryptoConnection:
    # this class does not provide 'response_io' interface
    def __init__(self,host,/,port=None,*,
                 hostname=None,tlsname=None,
                 context=None,timeout=15,blocksize=8192,
                 proxy_host=None,proxy_port=None,proxy_headers={},
                 dnsclient=None,certfilter=None,sni=True,uid=None):
        # args and kwds are totally same with http0.HTTPConnection
        # host and port used for target
        # hostname, tlsname and sni are ignored
        # context only used to detect https
        # others for remote

        # backend connection
        self._conn=None
        # remote infomation
        self.remote=Remote()
        self.remote.uid=uid
        # target infomation
        self.host=host
        self.port=port
        self.https=isinstance(context,SSLContext)
        # other params
        self.timeout=timeout
        self.blocksize=blocksize
        self.proxy_host=proxy_host
        self.proxy_port=proxy_port
        self.proxy_headers=proxy_headers
        self.dnsclient=dnsclient
        self.certfilter=certfilter
        self.uid=uid

        self._unpacker=None
        self._closed=True

        self.errmsg=b''
        self.status=0
        self.response_headers={}
        self.default_headers={}

    def __enter__(self):
        return self

    def __exit__(self,exc_type,exc_value,traceback):
        self.close()

    @property
    def remote_site(self):
        '''Hostname of target.'''
        return self.host

    @property
    def headers(self):
        '''Short cut of response_headers.'''
        return dict((k,v) for k,v in self.response_headers.items())

    @property
    def closed(self):
        return self._closed

    def setremote(self,host,/,port=None,*,path='/',hostname=None,tlsname=None,
                  context=None,sni=True,pubkey=None,
                  client_prikey=None,signature=None):
        self.remote.host=host
        self.remote.port=port
        self.remote.path=path
        self.remote.hostname=hostname
        self.remote.tlsname=tlsname
        self.remote.pubkey=pubkey
        self.remote.https=isinstance(context,SSLContext)
        if self.remote.https:
            self.remote.context=context
        self.remote.sni=sni
        self.remote.client_prikey=client_prikey
        self.remote.signature=signature

    def connect(self):
        if not self.remote.host:
            raise RuntimeError('remote not set.')
        self._conn=HTTPConnection(
            self.remote.host,port=self.remote.port,
            hostname=self.remote.hostname,tlsname=self.remote.tlsname,
            timeout=self.timeout,blocksize=self.blocksize,
            context=self.remote.context,
            proxy_host=self.proxy_host,proxy_port=self.proxy_port,
            proxy_headers=self.proxy_headers,
            dnsclient=self.dnsclient,certfilter=self.certfilter,
            sni=self.remote.sni,uid=self.uid)
        self._conn.default_headers['transfer-encoding']='chunked'
        self._conn.connect()
        if self._conn.status>599:
            # connect failed
            status=self._conn.status
            errmsg=self._conn.errmsg
            return self._set_failed(status,errmsg)
        self._closed=False

    def close(self):
        self.status=0
        self._unpacker=None
        self.response_headers.clear()
        if self._closed:
            return
        if self._conn is not None:
            self._conn.default_headers['connection']='close'
            self.request('','',closereq=True)
            self._close_remote()
            self.remote.close()
        self._closed=True

    def _set_interrupt(self,status,msg):
        # used to interrupt remote
        self.close()
        self.status=status
        self.errmsg=msg
        self.response_headers['content-length']=len(self.errmsg)
        self._unpacker=iterbytes(self.errmsg or b'unknown')

    def _set_failed(self,status,msg):
        # only used if remote down
        self._close_remote()
        self._set_interrupt(status,msg)

    def _close_remote(self):
        if self._conn is not None:
            conn=self._conn
            self._conn=None
            conn.close()

    def _create_auth(self):
        signature=self.remote.signature
        if isinstance(signature,str):
            signature=bytes.fromhex(signature)
        client_pubkey=crypto.generate_pubkey(self.remote.client_prikey)
        token=_park_token(client_pubkey,signature)
        return crypto.encrypt1(token,self.remote.pubkey)

    def _request_iter(self,urldata,headers,body):
        # urldata is tuple of (method,scheme,hostname,port,path,query,uid)
        yield self._create_auth()
        if isinstance(urldata,bytes):
            yield crypto.encrypt2(
                urldata,self.remote.client_prikey,self.remote.pubkey)
            return
        yield crypto.encrypt2(dumps(urldata,separators=(',',':')).encode('utf8'),
                              self.remote.client_prikey,self.remote.pubkey)
        yield crypto.encrypt2(dumps(headers,separators=(',',':')).encode('utf8'),
                              self.remote.client_prikey,self.remote.pubkey)
        if body is None:return
        bodys=(iterbytes(body,blocksize=1024) if isinstance(body,bytes) else body)
        for block in bodys:
            if not block:continue
            yield crypto.encrypt2(
                block,self.remote.client_prikey,self.remote.pubkey)

    def response_iter(self,blocksize=4096):
        # blocksize is ignored
        if self._unpacker is None:return
        try:
            yield from self._unpacker
        except Exception as e:
            raise Exception(f'unpack failed: {e}')
        finally:
            self._unpacker=None

    def request(self,method,path,/,*,body=None,headers={},closereq=False):
        if self._conn is None:
            self.connect()
        if self.status>599:
            # failed in connect
            return
        remote_path=(PurePosixPath(self.remote.path)/_randstr()).as_posix()
        if closereq:
            urldata=b'close'+self.uid.encode('utf8')
        else:
            ppath=urlparse(path)
            scheme='https' if self.https else 'http'
            urldata=(method,scheme,self.host,self.port,
                     ppath.path,ppath.query,self.uid)
        try:
            self._conn.request('POST',remote_path,
                               body=self._request_iter(urldata,headers,body),
                               encode_chunked=True)
        except Exception as e:
            return self._set_failed(620,f'remote down: {e}'.encode('utf8'))
        if closereq:
            return
        if self._conn.status!=200 and self._conn.status<600:
            return self._set_failed(
                621,f'remote error: {self._conn.status}'.encode('utf8'))
        self.response_headers.clear()
        decryptor=partial(crypto.decrypt2,
                          prikey=self.remote.client_prikey,
                          pubkey=self.remote.pubkey)
        unpacker=stream_unpack(self._conn.response_io,unpacker=decryptor)

        try:
            block0=next(unpacker)
            block1=next(unpacker)
        except Exception as e:
            return self._set_interrupt(622,f'unpack failed: {e}'.encode('utf8'))

        try:
            self.status=int(block0)
        except Exception as e:
            return self._set_interrupt(
                623,f'block0 failed: {e} {block0}'.encode('utf8'))

        try:
            self.response_headers.update(loads(block1))
        except Exception as e:
            return self._set_interrupt(
                624,f'block1 failed: {e} {block1}'.encode('utf8'))

        self._unpacker=unpacker

# Local Variables:
# coding: utf-8
# mode: python
# python-indent-offset: 4
# indent-tabs-mode: nil
# End:
