#  This file is part of proxpy.
#
#  proxpy is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  proxpy is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with proxpy.  If not, see <https://www.gnu.org/licenses/>.

_RCODE_MAP={
    0:('NoError','No Error'),
    1:('FormErr','Format Error'),
    2:('ServFail','Server Failure'),
    3:('NXDomain','Non-Existent Domain'),
    4:('NotImp','Not Implemented'),
    5:('Refused','Query Refused'),
    6:('YXDomain','Name Exists when it should not'),
    7:('YXRRSet','RR Set Exists when it should not'),
    8:('NXRRSet','RR Set that should exist does not'),
    9:('NotAuth','Server Not Authoritative for zone'),
    9:('NotAuth','Not Authorized'),
    10:('NotZone','Name not contained in zone'),
    11:('DSOTYPENI','DSO-TYPE Not Implemented'),
    16:('BADVERS','Bad OPT Version'),
    16:('BADSIG','TSIG Signature Failure'),
    17:('BADKEY','Key not recognized'),
    18:('BADTIME','Signature out of time window'),
    19:('BADMODE','Bad TKEY Mode'),
    20:('BADNAME','Duplicate key name'),
    21:('BADALG','Algorithm not supported'),
    22:('BADTRUNC','Bad Truncation'),
    23:('BADCOOKIE','Bad/missing Server Cookie'),
}

def rcode_str(rcode):
    try:
        return _RCODE_MAP[rcode]
    except KeyError:
        return 'UNKNOWN',f'Unknown rcode: {rcode}'

# Local Variables:
# coding: utf-8
# mode: python
# python-indent-offset: 4
# indent-tabs-mode: nil
# End:
