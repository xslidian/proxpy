#  This file is part of proxpy.
#
#  proxpy is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  proxpy is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with proxpy.  If not, see <https://www.gnu.org/licenses/>.

from time import time

class IPAddr:
    def __init__(self,name,ip,ttl):
        self.ip=ip
        self.expired=time()+int(ttl)

    def __repr__(self):
        return f'<IpAnswer ip={self.ip}, expired={self.expired}>'

    @property
    def isexpired(self):
        return time()>self.expired

class IPResult:
    def __init__(self,name):
        self.name=name
        self.answers={}

    def __repr__(self):
        return f'<IPResult name={self.name}, answers={self.answers!r}>'

    def __del__(self):
        self.answers.clear()

    def add(self,ip,ttl):
        self.answers[ip]=IPAddr(self.name,ip,ttl)

    def remove(self,ip):
        del self.answers[ip]

    def getip(self):
        for ip in set(self.answers.keys()):
            if self.answers[ip].isexpired:
                self.remove(ip)
        return list(self.answers.keys())

# Local Variables:
# coding: utf-8
# mode: python
# python-indent-offset: 4
# indent-tabs-mode: nil
# End:
